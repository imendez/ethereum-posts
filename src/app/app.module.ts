import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {Web3Service} from './services/web3.service';
import {ContractService} from './services/contract.service';
import {HttpClientModule} from '@angular/common/http';
import {HomeModule} from './components/home/home.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HomeModule
  ],
  providers: [Web3Service, ContractService],
  bootstrap: [AppComponent]
})
export class AppModule { }
