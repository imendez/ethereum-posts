import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as TruffleContract from 'truffle-contract';
import {Web3Service} from './web3.service';
import {Observable} from 'rxjs/Observable';
import {BigNumber} from 'bignumber.js';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/observable/range';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/reduce';

@Injectable()
export class ContractService {
  posts: any;
  account: any;
  private loaded: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private web3: Web3Service) {
    this.http.get('assets/contracts/Posts.json')
      .subscribe((response: any) => {
        this.posts = TruffleContract(response);
        this.posts.setProvider(web3.provider);
        web3.instance.eth.getAccounts((error, accounts) => {
          this.account = accounts[0];
          this.loaded.next(true);
        });
      });
  }

  getPost(index: number): Promise<string> {
    return new Promise((resolve) => {
      this
        .getContractInstance()
        .subscribe(instance => {
          resolve(instance.getPost(index));
        });
    });
  }

  getTotalPosts(): Observable<number> {
    return this
      .getContractInstance()
      .switchMap(instance => instance.getTotalPosts())
      .map((total: BigNumber) => total.toNumber());
  }

  getPosts(): Observable<string[]> {
    return this.getTotalPosts()
      .flatMap((total: number) => this.getPostsRange(0, total));
  }

  addPost(message: string): Observable<any> {
    return this
      .getContractInstance()
      .switchMap(instance => instance.addPost(message, {from: this.account}))
      .switchMap(response => Observable.of(response));
  }

  private getContractInstance(): Observable<any> {
    return this.loaded
      .filter((loaded: boolean) => loaded)
      .switchMap(() => this.posts.deployed());
  }

  private getPostsRange(start: number, end: number): Observable<string[]> {
    return Observable
      .range(start, end + 1)
      .concatMap((index) => this.getPost(index))
      .reduce((result, value) => {
        result.push(value);
        return result;
      }, []);
  }

}
