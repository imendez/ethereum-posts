import { Injectable } from '@angular/core';
import {default as Web3} from 'web3';

@Injectable()
export class Web3Service {

  provider: any;
  instance: Web3;

  constructor() {
    const web3 = window['web3'];
    if (web3) {
      this.provider = web3.currentProvider;
      this.instance = new window['Web3'](this.provider);
    }
  }

}
