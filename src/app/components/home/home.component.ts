import {Component, NgZone, OnInit} from '@angular/core';
import {Web3Service} from '../../services/web3.service';
import {ContractService} from '../../services/contract.service';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'posts-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  messages: any[] = [];
  form: FormGroup;

  get message(): AbstractControl {
    return this.form.get('message');
  }

  constructor(private web3Service: Web3Service, private contract: ContractService, private fb: FormBuilder, private zone: NgZone) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getPosts();
  }

  createForm() {
    this.form = this.fb.group({
      message: ['', [Validators.required]]
    });
  }

  submit() {
    this.contract.addPost(this.message.value)
      .subscribe((response) => {
        this.getPosts();
        this.zone.run(() => this.form.reset());
      });
  }

  getPosts() {
    this.contract.getPosts()
      .subscribe(posts => {
        this.zone.run(() => this.messages = posts);
      });
  }

}
