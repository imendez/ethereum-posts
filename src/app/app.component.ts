import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'posts-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  users: any[];

  constructor() {

  }

  ngOnInit(): void {
  }

}
