pragma solidity 0.4.18;


import ""

contract Posts is Ownable {

    string[] public messages;

    function addPost(string message) public returns(uint) {
        messages.push(message);
        return messages.length - 1;
    }

    function getTotalPosts() public view returns(uint) {
        return messages.length - 1;
    }

    function getPost(uint index) public view returns(string) {
        return messages[index];
    }

}
