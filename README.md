# Posts

An application that allows users to save messages to the ethereum blockchain.

## Developing

### Prerequisites

* Node.js >= 6.9.0
* npm >= 3

### Setting up Dev Environment

#### Installing dependencies

##### Installing truffle framework

```shell
npm install -g truffle
```

#### Obtaining the code from the repository

```shell
git clone https://gitlab.com/imendez/ethereum-posts
cd ethereum-posts
npm install
```

### Starting the project

#### Build and deploy the smart contract

Start the truffle development console and testrpc server

```shell
truffle develop
```

In the truffle console use the following commands

```shell
compile
migrate
```

#### Start the Angular Application

Copy the compiled contracts into the assets directory
```shell
cp -r ./build/contracts ./src/assets/
```

Start the application

```shell
npm start
```

## Configuration

